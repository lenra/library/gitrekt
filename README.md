# GitRekt

## About
This project is the extraction of the [Elixir GitGud project](https://github.com/almightycouch/gitgud) created by [redrabbit](https://github.com/redrabbit) under the MIT Licence.