defmodule GitRekt.MixProject do
  use Mix.Project

  @version "0.3.4"

  def project do
    [
      app: :gitrekt,
      version: @version,
      elixir: "~> 1.11",
      compilers: [:elixir_make] ++ Mix.compilers(),
      make_args: ["--quiet"],
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:elixir_make, "~> 0.6"},
      {:stream_split, "~> 0.1"},
      {:telemetry, "~> 0.4"},
      {:ex_doc, "~> 0.23", only: :dev}
    ]
  end

  defp docs do
    [
      main: "GitRekt",
      source_ref: "v#{@version}",
      source_url: "https://gitlab.com/lenra/platform/libs/gitrekt",
      groups_for_modules: [
        "Git low-level APIs": [
          GitRekt,
          GitRekt.Cache,
          GitRekt.Git,
          GitRekt.GitAgent,
          GitRekt.GitRepo,
          GitRekt.GitOdb,
          GitRekt.GitCommit,
          GitRekt.GitRef,
          GitRekt.GitTag,
          GitRekt.GitBlob,
          GitRekt.GitDiff,
          GitRekt.GitTree,
          GitRekt.GitTreeEntry,
          GitRekt.GitIndex,
          GitRekt.GitIndexEntry,
          GitRekt.Packfile,
          GitRekt.WireProtocol,
          GitRekt.WireProtocol.ReceivePack,
          GitRekt.WireProtocol.UploadPack
        ]
      ]
    ]
  end
end
